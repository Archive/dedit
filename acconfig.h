/* acconfig.h
   This file is in the public domain.

   Descriptive text for the C preprocessor macros that
   the distributed Autoconf macros can define.
   No software package will use all of them; autoheader copies the ones
   your configure.in uses into your configuration header file templates.

   The entries are in sort -df order: alphabetical, case insensitive,
   ignoring punctuation (such as underscores).  Although this order
   can split up related entries, it makes it easier to check whether
   a given entry is in the file.

   Leave the following blank line there!!  Autoheader needs it.  */

#undef DEDIT_CONFIGURATION
#undef DEBUG
#undef TRACE_FUNC
#undef GETTEXT_PACKAGE

#undef HAVE_LIBZ

#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_DIRENT_H
#undef HAVE_DOPRNT
#undef HAVE_GETTEXT
#undef HAVE_IPC_H
#undef HAVE_LC_MESSAGES
#undef HAVE_NDIR_H
#undef HAVE_PUTENV
#undef HAVE_SHM_H
#undef HAVE_STPCPY
#undef HAVE_SYS_DIR_H
#undef HAVE_SYS_NDIR_H
#undef HAVE_SYS_SELECT_H
#undef HAVE_SYS_TIME_H
#undef HAVE_UNISTD_H
#undef HAVE_VPRINTF
#undef HAVE_VSNPRINTF
#undef HAVE_XSHM_H

#undef HAVE_LIBSM

#undef IPC_RMID_DEFERRED_RELEASE

#undef NO_DIFFTIME
#undef NO_FD_SET

#undef RAND_FUNC
#undef SRAND_FUNC

#undef USE_PTHREADS

/* Leave that blank line there!!  Autoheader needs it.
   If you're adding to this file, keep in mind:
   The entries are in sort -df order: alphabetical, case insensitive,
   ignoring punctuation (such as underscores).  */
