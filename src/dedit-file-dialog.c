/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 1999,2000,20001  Takuo KITAME
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License with
 * the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307  USA
 *
 * DEditDocument Class
 *   Handling Documnet Text which has widget and status. and some methods.
 *
 * Author:
 *   Takuo KITAME <kitame@northeye.org>
 *
 */

#include <config.h>

#include <gtk/gtkfilesel.h>
#include <gtk/gtkdialog.h>

#include "dedit-file-dialog.h"

#define PARENT_TYPE GTK_TYPE_FILE_SELECTION

static GtkFileSelectionClass * parent_class = NULL;

struct _DEditFileDialogPrivate {
	GtkWidget * hbox;
	GtkWidget * label;
	GtkWidget * option_menu;
	GtkWidget * load_menu;

	DEditCharset charset;
};

/* Object Functions */
#if 0
enum { LAST_SIGNAL };
static guint signals[LAST_SIGNAL];
#endif

static void
destroy (GtkObject * object)
{
	DEditFileDialog * dialog;
	DEditFileDialogPrivate * priv;

	TRACE_ENTER;

	dialog = D_FILE_DIALOG (object);
	priv = dialog->priv;

	if (priv->label) {
		gtk_widget_destroy (priv->label);
		priv->label = NULL;
	}
	if (priv->load_menu) {
		gtk_widget_destroy (priv->load_menu);
		priv->load_menu = NULL;
	}
	if (priv->option_menu) {
		gtk_widget_destroy (priv->option_menu);
		priv->option_menu = NULL;
	}
	if (priv->hbox) {
		gtk_widget_destroy (priv->hbox);
		priv->hbox = NULL;
	}
	
	g_free (priv);

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
	
	TRACE_EXIT;
	return;
}

static void
class_init (DEditFileDialogClass * klass)
{
	GtkObjectClass * object_class;

	TRACE_ENTER;

	object_class = (GtkObjectClass *)klass;

	object_class->destroy = destroy;

	parent_class = gtk_type_class (PARENT_TYPE);

	TRACE_EXIT;

	return;
}

static void
init (DEditFileDialog * dialog)
{
	DEditFileDialogPrivate * priv;

	TRACE_ENTER;

	priv = g_new (DEditFileDialogPrivate, 1);

	priv->hbox = NULL;
	priv->label = NULL;
	priv->option_menu = NULL;
	priv->load_menu = NULL;

	priv->charset = ASCII;

	dialog->priv = priv;
	
	TRACE_EXIT;
	
	return;
}

static DEditFileDialog *
construct (DEditFileDialog * dialog, const gchar * title)
{
	DEditFileDialogPrivate * priv;

	TRACE_ENTER;

	g_return_val_if_fail (dialog != NULL, NULL);
	g_return_val_if_fail (D_IS_FILE_DIALOG (dialog), NULL);

	priv = dialog->priv;

	gtk_window_set_title (GTK_WINDOW (GTK_FILE_SELECTION (dialog)), title);
//	gtk_dialog_set_has_separator (GTK_DIALOG (GTK_FILE_SELECTION (dialog)), FALSE);

	TRACE_EXIT;
	return dialog;
}

/* public */
GtkWidget *
dedit_file_dialog_new (const gchar * title)
{
	DEditFileDialog * dialog;

	TRACE_ENTER;

	dialog = gtk_type_new (d_file_dialog_get_type ());

	if (! dialog) {
		TRACE_EXIT;
		return NULL;
	}
	
	construct (dialog, title);

	TRACE_EXIT;
	return GTK_WIDGET(GTK_FILE_SELECTION (dialog));
}

DEditCharset
dedit_file_dialog_get_charset (DEditFileDialog * dialog)
{
	DEditCharset ret = ASCII;

	TRACE_ENTER;
	
	TRACE_EXIT;

	return ret;
}

void
dedit_file_dialog_set_charset (DEditFileDialog * dialog, DEditCharset charset)
{

	TRACE_ENTER;
	TRACE_EXIT;

	return;
}

GType d_file_dialog_get_type(void)
{
	static GType type = 0;
	if (!type) {
		static const GTypeInfo info =
			{
				sizeof (DEditFileDialogClass),
				NULL,
				NULL,
				(GClassInitFunc) class_init,
				NULL,
				NULL,
				sizeof (DEditFileDialog),
				0,
				(GInstanceInitFunc) init,
			};
		type = g_type_register_static (PARENT_TYPE, "DEditFileDialog", &info, 0);
	}
	return type;
}

// D_MAKE_TYPE (d_file_dialog, "DEditFileDialog", DEditFileDialog, class_init, init, PARENT_TYPE)
