/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 1999,2000,20001  Takuo KITAME
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License with
 * the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307  USA
 *
 * DEditDocument Class
 *   Handling Documnet Text which has widget and status. and some methods.
 *
 * Author:
 *   Takuo KITAME <kitame@northeye.org>
 *
 */

#include <config.h>

#include <sys/stat.h>

#include "dedit-document.h"
#include "dedit-utils.h"

#define PARENT_TYPE GTK_TYPE_OBJECT

static GtkObjectClass * parent_class = NULL;

struct _DEditDocumentPrivate {
	GtkWidget * vbox;
    GtkWidget * scrolled_window;
    GtkWidget * text_view;
	GtkWidget * hruler;

	gboolean modified;
	gboolean read_only;

	gchar * filename; /* file name which given */
	gchar * name; /* child name same as basename of filename or untitled */
	
	gchar * path;     /* absolute file path (direname) */
	
	gchar * charset;
	gint charset_id;

	GnomeMDIGenericChild * mdi_generic_child;
};

enum {
	TEXT_SAVED,
	TEXT_MODIFID,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL];

static void
destroy (GtkObject *object)
{
	DEditDocument *document;
	DEditDocumentPrivate *priv;

	TRACE_ENTER;
	
	document = D_DOCUMENT (object);
	priv = document->priv;

	if (priv->name != NULL)
		g_free (priv->name);
	if (priv->path != NULL)
		g_free (priv->path);
	if (priv->filename != NULL)
		g_free (priv->filename);
	if (priv->charset != NULL)
		g_free (priv->charset);
	
	// g_free (priv);

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		GTK_OBJECT_CLASS (parent_class)->destroy (object);

	TRACE_EXIT;
	
	return;
}

static void
finalize (GObject *object)
{
	DEditDocument * document;

	g_return_if_fail(object != NULL);
	g_return_if_fail(D_IS_DOCUMENT(object));

	document = D_DOCUMENT (object);
	g_free (document->priv);
	document->priv = NULL;

	if (G_OBJECT_CLASS (parent_class)->finalize)
		(* G_OBJECT_CLASS(parent_class)->finalize) (object);
}

static void
class_init (DEditDocumentClass *klass)
{
	GObjectClass   *gobject_class;
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;
	
	TRACE_ENTER;

	gobject_class = (GObjectClass *) klass;
	object_class = (GtkObjectClass *) klass;
	widget_class = (GtkWidgetClass *) klass;

	parent_class = g_type_class_peek_parent (klass);
	
	object_class->destroy = destroy;
	gobject_class->finalize = finalize;


	TRACE_EXIT;
	
	return;
}

static void
init (DEditDocument *document)
{
	DEditDocumentPrivate *priv;

	TRACE_ENTER;
	
	priv = g_new0 (DEditDocumentPrivate, 1);

	priv->vbox = NULL;
	priv->text_view = NULL;
	priv->scrolled_window = NULL;
	priv->hruler = NULL;
	
	priv->filename = NULL;
	priv->name = NULL;
	priv->path = NULL;

	priv->charset = NULL;

	priv->modified = FALSE;
	priv->read_only = FALSE;
	
	document->priv = priv;

	TRACE_EXIT;

	return;
}

static void
setup_widgets (DEditDocument *document)
{
	DEditDocumentPrivate *priv;
	GtkTextBuffer * buffer;
	GtkTextIter iter;

	TRACE_ENTER;

	priv = document->priv;

	priv->vbox = gtk_vbox_new (FALSE, 0);

	gtk_container_border_width (GTK_CONTAINER(priv->vbox), 1);

	priv->scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (priv->scrolled_window),
									GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

/* Ruler */
#if 0
	junk = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (priv->vbox), junk, FALSE, FALSE, 0);
	
	priv->hruler = gtk_hruler_new ();
	gtk_ruler_set_range (GTK_RULER (priv->hruler), 0.0, 100.0, 1.0, 100.0);
	gtk_box_pack_start (GTK_BOX (junk), priv->hruler, TRUE, TRUE, 1);
	gtk_widget_set_usize (junk, 1, -1);
#endif
	
	priv->text_view = gtk_text_view_new ();
	gtk_text_view_set_editable (GTK_TEXT_VIEW (priv->text_view), TRUE);
	gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (priv->text_view), TRUE);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->text_view));
	gtk_text_buffer_get_iter_at_offset (buffer, &iter, 0);
	gtk_text_buffer_place_cursor (buffer, &iter);

	gtk_container_add (GTK_CONTAINER (priv->scrolled_window),
					   priv->text_view);

	gtk_box_pack_start (GTK_BOX (priv->vbox), priv->scrolled_window,
						TRUE, TRUE, 0);

	gtk_widget_show_all (priv->vbox);
	gtk_widget_grab_focus (GTK_WIDGET (priv->text_view));
	/* Try to have a visible cursor */
	/* FIXME: Remove this dirty hack to have the cursor visible when we will 
	 * understand why it is not visible */
	
	GTK_WIDGET_SET_FLAGS (GTK_WIDGET (priv->text_view), GTK_HAS_FOCUS);
	g_object_set (G_OBJECT (priv->text_view), "cursor_visible", FALSE, NULL);
	g_object_set (G_OBJECT (priv->text_view), "cursor_visible", TRUE, NULL);
	
	TRACE_EXIT;
	
	return;
}

static GtkWidget *
create_view (GnomeMDIChild * child, gpointer data)
{
	DEditDocumentPrivate * priv;
	DEditDocument * document = D_DOCUMENT (data);

	TRACE_ENTER;
	g_object_ref (document);
	priv = document->priv;

	if (priv->vbox == NULL)
		setup_widgets (document);

	gnome_mdi_child_set_name (GNOME_MDI_CHILD (child), priv->name);

	TRACE_EXIT;
	
	return priv->vbox;
}

static DEditDocument *
construct (DEditDocument *document, const gchar *filename)
{
	DEditDocumentPrivate *priv;
		
	TRACE_ENTER;

	g_return_val_if_fail (document != NULL, NULL);
	g_return_val_if_fail (D_IS_DOCUMENT (document), NULL);

	priv = document->priv;

	if (filename) {
		dedit_document_set_file_name (document, filename);
	} else {
		priv->filename = NULL;
		priv->path = NULL;
		priv->name = NULL;
	}

	TRACE_ENTER;
	
	return document;
}
/* end of Widget Class */

/* private */

/* public */
void
dedit_document_set_file_name (DEditDocument *document, const gchar * filename)
{
	DEditDocumentPrivate * priv;
	struct stat st;

	g_return_if_fail (D_IS_DOCUMENT (document));
	g_return_if_fail (filename != NULL);
	
	TRACE_ENTER;

	priv = document->priv;

	if (priv->filename != NULL) g_free (priv->filename);
	if (priv->name != NULL) g_free (priv->name);
	if (priv->path != NULL) g_free (priv->path);

	priv->filename = g_strdup (filename);
	priv->name = g_basename (filename);
	priv->path = g_get_absolute_file_path (filename);

	/* FIXME: need critical assertion ?*/
	g_return_if_fail (priv->name != NULL);
	g_return_if_fail (priv->path != NULL);

	if (stat (filename, &st) != -1) {
		if (access (filename, W_OK) == -1)
			priv->read_only = TRUE;
		else
			priv->read_only = FALSE;
	} 
	
	TRACE_EXIT;

	return;
}

DEditDocument *
dedit_document_new (void)
{
	DEditDocument *document;

	TRACE_ENTER;

	document = D_DOCUMENT (g_object_new (D_DOCUMENT_TYPE, NULL));
	
	if (! document) {
		TRACE_EXIT;
		return NULL;
	}
	
	construct (document, NULL);

	TRACE_EXIT;
	
	return document;
}

DEditDocument *
dedit_document_new_with_file (const gchar *filename)
{
	DEditDocument *document;

	TRACE_ENTER;

	g_return_val_if_fail (filename != NULL, NULL);
	
	document = gtk_type_new (d_document_get_type ());
	
	if (! document) {
		TRACE_EXIT;
		return NULL;
	}
	
	construct (document, filename);

	TRACE_EXIT;
	
	return document;
}

gboolean
dedit_document_is_modified (DEditDocument *document)
{
	DEditDocumentPrivate *priv;

	TRACE_ENTER;

	g_return_val_if_fail (D_IS_DOCUMENT (document), FALSE);

	priv = document->priv;

	TRACE_EXIT;
	
	return priv->modified;
}

const gchar *
dedit_document_get_name (DEditDocument *document)
{
	DEditDocumentPrivate *priv;

	g_return_val_if_fail (D_IS_DOCUMENT (document), NULL);

	priv = document->priv;
	
	return priv->name;
}

const gchar *
dedit_document_get_path (DEditDocument *document)
{
	DEditDocumentPrivate *priv;

	g_return_val_if_fail (D_IS_DOCUMENT (document), NULL);

	priv = document->priv;
	
	return priv->path;
}

/* cut buffer handler */
void
dedit_document_cut_clipboard (DEditDocument * document)
{
	DEditDocumentPrivate *priv;
	GtkTextBuffer * buffer;
	
	TRACE_ENTER;
	
	g_return_if_fail (D_IS_DOCUMENT (document));

	priv = document->priv;
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->text_view));

	gtk_text_buffer_cut_clipboard (buffer, gtk_clipboard_get (GDK_NONE), TRUE);

	TRACE_EXIT;
	
	return;
}

void
dedit_document_copy_clipboard (DEditDocument * document)
{
	DEditDocumentPrivate * priv;
	GtkTextBuffer * buffer;

	TRACE_ENTER;
	
	g_return_if_fail (D_IS_DOCUMENT (document));

	priv = document->priv;
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->text_view));

	gtk_text_buffer_copy_clipboard (buffer, gtk_clipboard_get (GDK_NONE));

	TRACE_EXIT;
	
	return;
}

void
dedit_document_paste_clipboard (DEditDocument * document)
{
	DEditDocumentPrivate *priv;
	GtkTextBuffer *buffer;
	
	TRACE_ENTER;
	
	g_return_if_fail (D_IS_DOCUMENT (document));

	priv = document->priv;

	priv = document->priv;
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->text_view));

	gtk_text_buffer_paste_clipboard (buffer,
									 gtk_clipboard_get (GDK_NONE),
									 NULL, TRUE);

	TRACE_EXIT;
	
	return;
}

void
dedit_document_select_all (DEditDocument * document)
{
	DEditDocumentPrivate * priv;
	GtkTextBuffer * buffer;
	GtkTextIter start_iter, end_iter;

	TRACE_ENTER;
	
	g_return_if_fail (D_IS_DOCUMENT (document));

	priv = document->priv;
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->text_view));
	gtk_text_buffer_get_start_iter (buffer, &start_iter);
	gtk_text_buffer_get_end_iter (buffer, &end_iter);
	gtk_text_buffer_place_cursor (buffer, &end_iter);

	gtk_text_buffer_move_mark (buffer,
                               gtk_text_buffer_get_mark (buffer,
														 "selection_bound"),
                               &start_iter);

	TRACE_EXIT;
	
	return;
}

void
dedit_document_create_mdi_child (DEditDocument * document, GnomeMDI * mdi)
{
	DEditDocumentPrivate *priv;
	static guint i = 0; 

	TRACE_ENTER;
	
	g_return_if_fail (D_IS_DOCUMENT (document));
	g_return_if_fail (GNOME_IS_MDI (mdi));

	priv = document->priv;
	
	
	if (priv->filename == NULL) {
		g_assert (priv->name == NULL);
		priv->name = g_strdup_printf ("%s[%d]", _("Untitled"), i++);
	}

	priv->mdi_generic_child = gnome_mdi_generic_child_new (priv->name);
	gnome_mdi_generic_child_set_view_creator (priv->mdi_generic_child,
											  create_view, document);
	gtk_object_set_data (GTK_OBJECT (priv->mdi_generic_child),
						 "DEditDocument", document);
	
	gnome_mdi_add_child (mdi, GNOME_MDI_CHILD (priv->mdi_generic_child));
	gnome_mdi_add_view  (mdi, GNOME_MDI_CHILD (priv->mdi_generic_child));

	TRACE_EXIT;

	return;
}

void
dedit_document_grab_focus (DEditDocument * document)
{
	DEditDocumentPrivate * priv;

	TRACE_ENTER;

	g_return_if_fail (D_IS_DOCUMENT (document));

	priv = document->priv;

	gtk_widget_grab_focus (priv->text_view);
	
	TRACE_EXIT;

	return;
}

/* File handler */

#include <zlib.h>
#include <libgnomevfs/gnome-vfs.h>

#define PREALLOCATED_BUF_LEN 4096

#define GZIP_FILE_MIME_TYPE "application/x-gzip"
#define TEXT_FILE_MIME_TYPE "text/plain"

static GString *
read_gz_file (const gchar * filename)
{
	GString * str;
	gchar buf[BUFSIZ];
	gzFile * zfp;

	TRACE_ENTER;

	if ((zfp = gzopen (filename, "r")) == NULL) {
		TRACE_EXIT;
		return NULL;
	}

	str = g_string_sized_new (BUFSIZ + 1);
	while (gzgets (zfp, buf, sizeof (BUFSIZ))) {
		g_string_append(str, buf);
	}

	gzclose (zfp);
	
	TRACE_EXIT;
	
	return str;
}

static gboolean
save_file_gz (GtkWidget * text, const gchar * filename, gboolean backup)
{
	TRACE_ENTER;
	TRACE_EXIT;

	return TRUE;
}

static gboolean
save_file (GtkWidget * text, const gchar * filename, gboolean backup)
{
	gchar backup_file[PATH_MAX];
	gboolean backuped = FALSE;
	gchar * buffer;
	struct stat st;
	FILE * fp;

	TRACE_ENTER;

	if (stat (filename, &st) != -1) {
		if (access (filename, W_OK) == -1) {
            g_warning(_ ("DEdit: %s: Permission denied\n"), filename);
            return FALSE;
        }
		g_snprintf (backup_file, sizeof (backup_file), "%s~", filename);
		unlink (backup_file);
		rename (filename, backup_file);
		backuped = TRUE;
	}

	if ((fp = fopen (filename, "w")) == NULL) {
		g_warning (_ ("DEdit: %s: Cannot open file to write.\n"), filename);
		if (backuped) {
			rename (backup_file, filename);
			unlink (backup_file);
		}
		return FALSE;
	}

	buffer = g_strdup (gtk_editable_get_chars (GTK_EDITABLE (text), 0, -1));

	fprintf (fp, "%s", buffer);
	fclose (fp);

	if (backuped) {
		chmod (filename, st.st_mode);
	}
	
	TRACE_EXIT;

	return TRUE;
}

static void
load_file (GtkWidget * text_view, const gchar * filename, gboolean read_only)
{
	GnomeVFSFileInfo *info;
	GnomeVFSHandle *from_handle;
	GnomeVFSResult  result;

	GnomeVFSURI * uri;
	const gchar * scheme;
	
	GString * tmp_buf = NULL;

	GtkTextBuffer *buffer;
	
	TRACE_ENTER;

	g_return_if_fail (GTK_IS_TEXT_VIEW (text_view));
	g_return_if_fail (filename != NULL);
	
	info = gnome_vfs_file_info_new ();
	result = gnome_vfs_get_file_info (filename, info,
									  (GNOME_VFS_FILE_INFO_GET_MIME_TYPE
									   | GNOME_VFS_FILE_INFO_FOLLOW_LINKS));
	if ((result != GNOME_VFS_OK) || 
		!(info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_TYPE) ||
		(info->type != GNOME_VFS_FILE_TYPE_REGULAR)) {
		/* FIXME: should use GUI */
		g_warning (_ ("An error was encountered while opening the file \"%s\"."
					"\nPlease make sure the file exists.\n"), filename);
		if (result == GNOME_VFS_OK)
			gnome_vfs_file_info_clear (info);

		TRACE_EXIT;
		return;
	}

	if (strcasecmp (info->mime_type, GZIP_FILE_MIME_TYPE) == 0) {
		tmp_buf = read_gz_file (filename);
		if (!tmp_buf) {
			/* FIXME: should use GUI */
			g_warning (_("An error was encountered while reading the file: \n\n%s\n\n"
					   "Make sure that you have permissions for opening the file. \n"), filename);
			gnome_vfs_file_info_clear (info);
			TRACE_EXIT;			
			return;
		}
	} else {
		if (info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_SIZE) {
			tmp_buf = g_string_sized_new (info->size + 1);
		} else {
			tmp_buf = g_string_sized_new (PREALLOCATED_BUF_LEN);
		}
		if (tmp_buf == NULL) {
		/* FIXME: should use GUI */
			g_warning (_("An error was encountered while opening the file \"%s\"."
					   "\nCould not allocate the required memory.\n"), filename);
			gnome_vfs_file_info_clear (info);
			TRACE_EXIT;
			return;
		}		
		
		result = gnome_vfs_open (&from_handle, filename, GNOME_VFS_OPEN_READ);

		if (result != GNOME_VFS_OK)	{
			/* FIXME: should use GUI */
			g_warning (_("An error was encountered while reading the file: \n\n%s\n\n"
					   "Make sure that you have permissions for opening the file. \n"), filename);
			g_string_free (tmp_buf, TRUE);
			gnome_vfs_file_info_clear (info);
			TRACE_EXIT;
			return;
		}
		
		while (TRUE) {
			GnomeVFSFileSize bytes_read;
			guint8           data [1025];
			
			result = gnome_vfs_read (from_handle, data, 1024, &bytes_read);
			
			if ((result != GNOME_VFS_OK) && (result != GNOME_VFS_ERROR_EOF)) {
				/* FIXME: should use GUI */
				g_warning (_("An error was encountered while reading the file: \n\n%s\n"), filename);
				g_string_free (tmp_buf, TRUE);
				gnome_vfs_file_info_clear (info);
				TRACE_EXIT;
				return;
			}
			if (bytes_read == 0) {
				break;
			}
			if (bytes_read >  0 && bytes_read <= 1024) {
				data [bytes_read] = '\0';
			} else {
				/* FIXME: should use GUI */
				g_warning (_("Internal error reading the file: \n\n%s\n\n"
						   "Please, report this error to kitame@northeye.org.\n"), filename);
				g_string_free (tmp_buf, TRUE);
				gnome_vfs_file_info_clear (info);
				TRACE_EXIT;
				return;
			}			
			g_string_append (tmp_buf, data);
		} /* while (TRUE) */
		
		result = gnome_vfs_close (from_handle);
        
		if (result != GNOME_VFS_OK) {
			/* FIXME: should use GUI */
			g_warning (_("An error was encountered while closing the file: \n\n%s\n"), filename);
			g_string_free (tmp_buf, TRUE);
			gnome_vfs_file_info_clear (info);
			TRACE_EXIT;
			return;
		}
	}
	
	uri = gnome_vfs_uri_new (filename);
	
	if (uri != NULL) {
		scheme = gnome_vfs_uri_get_scheme (uri);
		/* FIXME: all remote files are marked as readonly */
		if ((scheme != NULL) && (strcmp (scheme, "file") == 0) &&
			GNOME_VFS_FILE_INFO_LOCAL (info)) {
			gchar * tmp_str;
			gchar * tmp_str2;
			
			tmp_str = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD);                            
			tmp_str2 = gnome_vfs_unescape_string_for_display (tmp_str);
			
			if (tmp_str2 != NULL) {
				read_only = access (tmp_str2, W_OK) ? TRUE : FALSE;
			}
			g_free (tmp_str2);
			g_free (tmp_str);
		}
		gnome_vfs_uri_unref (uri);
	}

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (text_view));
	// gtk_text_set_point (GTK_TEXT (text), 0);
	// gtk_text_forward_delete (GTK_TEXT (text), -1);
	gtk_text_buffer_set_text (buffer, tmp_buf->str, -1);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (text_view), !read_only);

	g_string_free (tmp_buf, TRUE);

	gnome_vfs_file_info_clear (info);
	
	TRACE_EXIT;

	return;
}

void
dedit_document_load_file (DEditDocument * document)
{
	DEditDocumentPrivate * priv;
	gchar filename [PATH_MAX];

	g_return_if_fail (D_IS_DOCUMENT (document));
	
	TRACE_ENTER;

	priv = document->priv;

	g_return_if_fail (priv->filename != NULL);
	g_return_if_fail (priv->path != NULL);
	g_return_if_fail (priv->name != NULL);

	g_snprintf (filename, sizeof (filename), "%s/%s", priv->path, priv->name);
	
	load_file (priv->text_view, filename, priv->read_only);

	TRACE_EXIT;

	return;
}

GType d_document_get_type(void)
{
	static GType type = 0;
	if (!type) {
		static const GTypeInfo info =
			{
				sizeof (DEditDocumentClass),
				NULL,
				NULL,
				(GClassInitFunc) class_init,
				NULL,
				NULL,
				sizeof (DEditDocument),
				0,
				(GInstanceInitFunc) init,
			};
		type = g_type_register_static (PARENT_TYPE, "DEditDocument", &info, 0);
	}
	return type;
}
