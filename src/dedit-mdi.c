/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 1999,2000,20001  Takuo KITAME
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License with
 * the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307  USA
 *
 * DEdit MDI part
 *
 * Author:
 *   Takuo KITAME <kitame@northeye.org>
 *
 */

#include <config.h>

#include <gnome.h>
#include <libgnomeui/gnome-window-icon.h>

#include "dedit-mdi.h"
#include "dedit-gconf.h"
#include "dedit-menus.h"
#include "dedit-utils.h"

static gboolean
remove_child_cb (GnomeMDI *mdi, GtkWidget * widget, gpointer data)
{
	GdkWindow *toplevel;
	gint root_x, root_y, x, y;
	
	TRACE_ENTER;
	
	if (mdi->active_window) {
		toplevel =
			gdk_window_get_toplevel (GTK_WIDGET (mdi->active_window)->window);
		gdk_window_get_root_origin (toplevel, &root_x, &root_y);
		/* We don't want to save the size of a maximized window,
		   so chek the left margin. This will not work if there is
		   a pannel in left, but there is no other way that I know
		   of knowing if a window is maximzed or not */
		if (root_x != 0)
			if (mdi->active_window) {
				gdk_window_get_size (GTK_WIDGET (mdi->active_window)->window,
									 &x, &y);
				
				dedit_gconf_set_int ("width", (gint) x);
				dedit_gconf_set_int ("height", (gint) y);
			}
	}

	TRACE_EXIT;

	return TRUE;
}

static gboolean
add_view_cb (GnomeMDI *mdi, GtkWidget * widget, gpointer data)
{
	
	return TRUE;
}

static void
destroy_cb (GnomeMDI * mdi, gpointer data)
{
	TRACE_ENTER;

	gtk_main_quit ();

	TRACE_EXIT;

	return;
};
static void
view_changed_cb (GnomeMDI *mdi, GtkWidget * widget, gpointer data)
{
	GtkWidget * statusbar;
	GtkWidget * modifiedbar;
	GtkWidget * charsetbar;
	
	DEditDocument * document;

	TRACE_ENTER;

	statusbar = gtk_object_get_data (GTK_OBJECT (mdi->active_window), "status");
	modifiedbar = gtk_object_get_data (GTK_OBJECT (mdi->active_window), "modified");
	charsetbar = gtk_object_get_data (GTK_OBJECT (mdi->active_window), "charset");
	document = dedit_mdi_get_active_document (mdi);

	if (dedit_document_get_path (document)) {
		gchar stat[PATH_MAX];
		g_snprintf (stat, sizeof (stat), "%s/%s",
					dedit_document_get_path (document),
					dedit_document_get_name (document));
		gnome_appbar_set_default (GNOME_APPBAR (statusbar), stat);
	} else if (dedit_document_get_name (document)) {
		gnome_appbar_set_default (GNOME_APPBAR (statusbar),
									dedit_document_get_name (document));
	}
	
	gtk_widget_set_sensitive (GTK_WIDGET (modifiedbar),
							  dedit_document_is_modified (document));
	gtk_statusbar_pop (GTK_STATUSBAR (charsetbar), 1);
	gtk_statusbar_push (GTK_STATUSBAR (charsetbar), 1, _("ASCII"));	
	
	dedit_document_grab_focus (document);
	
	TRACE_EXIT;
	
	return;
};

static void
app_created_cb (GnomeMDI *mdi, GnomeApp *app, gpointer data)
{
	GtkWidget * statusbar;
	GtkWidget * modifiedbar = gtk_statusbar_new ();
	GtkWidget * charsetbar = gtk_statusbar_new ();
	
	TRACE_ENTER;

	statusbar = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_USER);
	gnome_app_set_statusbar (app, GTK_WIDGET (statusbar));
	gnome_app_install_menu_hints (app, gnome_mdi_get_menubar_info (mdi->active_window));

	gtk_statusbar_push (GTK_STATUSBAR (modifiedbar), 1, _("Modified"));
	gtk_statusbar_push (GTK_STATUSBAR (charsetbar), 1, _("ASCII"));
	
	gtk_box_pack_start (GTK_BOX (GTK_HBOX (statusbar)), modifiedbar,
						FALSE, FALSE, 1);
    gtk_box_pack_start (GTK_BOX (GTK_HBOX (statusbar)), charsetbar,
						FALSE, FALSE, 1);
	gtk_widget_set_sensitive (GTK_WIDGET (modifiedbar), FALSE);
	
	gtk_window_set_default_size (GTK_WINDOW (app),
								 dedit_gconf_get_int ("width"),
								 dedit_gconf_get_int ("height"));

	gtk_window_set_policy (GTK_WINDOW (app), TRUE, TRUE, FALSE);

	gtk_object_set_data (GTK_OBJECT (app), "status", statusbar);
	gtk_object_set_data (GTK_OBJECT (app), "modified", modifiedbar);
	gtk_object_set_data (GTK_OBJECT (app), "charset", charsetbar);

	gtk_widget_show_all (GTK_WIDGET (modifiedbar));
	gtk_widget_show_all (GTK_WIDGET (charsetbar));

	TRACE_EXIT;

	return;
};

DEditDocument *
dedit_mdi_get_active_document (GnomeMDI * mdi)
{
	DEditDocument * document;	

	TRACE_ENTER;

	g_return_val_if_fail (GNOME_IS_MDI (mdi), NULL);
	g_return_val_if_fail (mdi->active_window != NULL, NULL);
	
	document = gtk_object_get_data (GTK_OBJECT (mdi->active_child),
									"DEditDocument");
	
	TRACE_EXIT;
	return document;
}

GnomeMDI *
dedit_mdi_init (const gchar * appname, const gchar * title)
{
	GnomeMDI * mdi;
	GnomeMDIMode mdi_mode [] = {
        GNOME_MDI_NOTEBOOK,
        GNOME_MDI_TOPLEVEL,
        GNOME_MDI_MODAL,
        GNOME_MDI_DEFAULT_MODE
	};
	GtkPositionType tab_pos [] = {
		GTK_POS_TOP,
		GTK_POS_LEFT,
		GTK_POS_RIGHT,
		GTK_POS_BOTTOM
	};

	TRACE_ENTER;
	
	g_assert (appname != NULL);
	g_assert (title != NULL);
	
    gnome_window_icon_set_default_from_file (GNOME_ICONDIR "/gnome-note.png");
    
	mdi = GNOME_MDI (gnome_mdi_new (appname, title));

	gnome_mdi_set_mode (mdi, mdi_mode[dedit_gconf_get_int ("mdi_mode")]);
	mdi->tab_pos = tab_pos[dedit_gconf_get_int ("mdi_tab_pos")];

	dedit_menus_init (mdi);

    gnome_mdi_set_child_menu_path (mdi, GNOME_MENU_FILE_STRING);
    gnome_mdi_set_child_list_path (mdi, _("_Documents/"));
    
    gtk_signal_connect (GTK_OBJECT (mdi), "remove_child",
                        GTK_SIGNAL_FUNC (remove_child_cb), NULL);

	gtk_signal_connect (GTK_OBJECT (mdi), "add_view",
                        GTK_SIGNAL_FUNC (add_view_cb), NULL);

    gtk_signal_connect (GTK_OBJECT (mdi), "destroy",
                        GTK_SIGNAL_FUNC (destroy_cb), NULL);
    
    gtk_signal_connect (GTK_OBJECT (mdi), "view_changed", 
                        GTK_SIGNAL_FUNC (view_changed_cb), NULL);

    gtk_signal_connect (GTK_OBJECT (mdi), "app_created", 
                        GTK_SIGNAL_FUNC (app_created_cb), NULL);
	
    gnome_mdi_open_toplevel (mdi);
	
	TRACE_EXIT;

	return mdi;
}
