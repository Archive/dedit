/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 1999,2000,20001  Takuo KITAME
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License with
 * the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307  USA
 *
 * Author:
 *   Takuo KITAME <kitame@northeye.org>
 *
 */

#ifndef __DEDIT_UTILS_H__
#define __DEDIT_UTILS_H__

#include <glib.h>
#include <gtk/gtkobject.h>

#ifdef TRACE_FUNC
#define T(x) g_print x
#define TRACE_ENTER g_print("*** TRACE-ENTER: %s(%d):%s\n",__FILE__, __LINE__, __FUNCTION__)
#define TRACE_EXIT g_print("*** TRACE-EXIT : %s(%d):%s\n",__FILE__, __LINE__, __FUNCTION__)
#else
#define T(x)
#define TRACE_ENTER
#define TRACE_EXIT
#endif
#ifdef DEBUG
#define D(x) g_message x
#else
#define D(x)
#endif

typedef enum {
	ASCII,
	EUC_JP,
	ISO_2022_JP,
	SHIFT_JIS,
	AUTO_JA
} DEditCharset;

gchar * g_get_absolute_file_name (const gchar * filename);
gchar * g_get_absolute_file_path (const gchar * filename);

#endif /* __DEDIT_UTILS_H__ */
