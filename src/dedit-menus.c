/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 1999,2000,20001  Takuo KITAME
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License with
 * the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307  USA
 *
 * MENU and TOOLBAR
 *
 * Author:
 *   Takuo KITAME <kitame@northeye.org>
 *
 */

#include <config.h>

#include "dedit-menus.h"
#include "dedit-mdi.h"
#include "dedit-file-dialog.h"
#include "dedit-utils.h"

/* callbacks */
/* File */
static void
new_cb (GtkWidget * widget, gpointer data)
{
    GnomeMDI * mdi = GNOME_MDI (data);
    DEditDocument * document;

    TRACE_ENTER;

    document = dedit_document_new ();
    dedit_document_create_mdi_child (document, mdi);
    
    TRACE_EXIT;

    return;
}

static void
open_cb (GtkWidget * widget, gpointer data)
{
	GtkWidget * dialog;
    TRACE_ENTER;

	dialog = dedit_file_dialog_new (_ ("DEdit: Open File"));
	gtk_widget_show (dialog);

    TRACE_EXIT;
    return;
}

static void
save_cb (GtkWidget * widget, gpointer data)
{
    TRACE_ENTER;

    TRACE_EXIT;    
    return;
}

static void
save_as_cb (GtkWidget * widget, gpointer data)
{
    TRACE_ENTER;

    TRACE_EXIT;    
    return;
}

static void
print_cb (GtkWidget * widget, gpointer data)
{
    TRACE_ENTER;

    TRACE_EXIT;    
    return;
}

static void
close_cb (GtkWidget * widget, gpointer data)
{
    GnomeMDI * mdi = GNOME_MDI (data); 

    TRACE_ENTER;

	if (mdi->active_view) {
		gnome_mdi_remove_child (mdi,
                                gnome_mdi_get_child_from_view (mdi->active_view),
                                FALSE);
    }

#if 0
    /* FIXME: Is it needed? */
    if (g_list_length (mdi->children) == 0)
        quit_cb (widget, data);
#endif
    
    TRACE_EXIT;    
    return;
}

static void
quit_cb (GtkWidget * widget, gpointer data)
{
    GnomeMDI * mdi = GNOME_MDI (data);
    TRACE_ENTER;

	if (gnome_mdi_remove_all (mdi, FALSE))
		gtk_object_destroy (GTK_OBJECT (mdi));

    TRACE_EXIT;

    return;
}

/* Edit */
static void
cut_cb (GtkWidget * widget, gpointer data)
{
    GnomeMDI * mdi = GNOME_MDI (data);
    DEditDocument * document;
    
    TRACE_ENTER;

    document = dedit_mdi_get_active_document (mdi);
    if (document)
        dedit_document_cut_clipboard (document);

    TRACE_EXIT;

    return;
}

static void
copy_cb (GtkWidget * widget, gpointer data)
{
    GnomeMDI * mdi = GNOME_MDI (data);
    DEditDocument * document;
    
    TRACE_ENTER;

    document = dedit_mdi_get_active_document (mdi);
    if (document)
        dedit_document_copy_clipboard (document);

    TRACE_EXIT;

    return;
}

static void
paste_cb (GtkWidget * widget, gpointer data)
{
    GnomeMDI * mdi = GNOME_MDI (data);
    DEditDocument * document;
    
    TRACE_ENTER;

    document = dedit_mdi_get_active_document (mdi);
    if (document)
        dedit_document_paste_clipboard (document);

    TRACE_EXIT;

    return;
}

static void
select_all_cb (GtkWidget * widget, gpointer data)
{
    GnomeMDI * mdi = GNOME_MDI (data);
    DEditDocument * document;
    
    TRACE_ENTER;

    document = dedit_mdi_get_active_document (mdi);
    if (document)
        dedit_document_select_all (document);

    TRACE_EXIT;

    return;
}

static void
prefs_cb (GtkWidget * widget, gpointer data)
{
    TRACE_ENTER;
    TRACE_EXIT;
    return;
}

/* Help */
static void
about_cb (GtkWidget * widget, gpointer data)
{
    GtkWidget * about;
    const gchar * authors[] = {
        _ ("Takuro KITAME  (main program)"),
        NULL
    };
    const gchar * comments[] = {
        NULL
    };
      
    TRACE_ENTER;

    about = gnome_about_new ("DEdit2", 
                             VERSION,
                             _ ("Copyright (C) 2002 Takuo KITAME."),
							 _ ("DEdit The Simple Editor for Beginners."),
                             authors,
                             comments,
							 "",
                             NULL);
    
    gtk_window_set_modal (GTK_WINDOW (about), TRUE);
    gtk_window_position (GTK_WINDOW (about), GTK_WIN_POS_MOUSE);
    gtk_widget_show (about);

    TRACE_EXIT;

    return;
}

/* define */
#include "menu-defs.h"

void
dedit_menus_init (GnomeMDI * mdi)
{
    TRACE_ENTER;

    gnome_mdi_set_menubar_template (mdi, mainmenu);
    gnome_mdi_set_toolbar_template (mdi, toolbar);
    
    TRACE_EXIT;

    return;
}
