/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 1999,2000,20001  Takuo KITAME
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License with
 * the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307  USA
 *
 * _DESCRIPTION_
 *
 * Author:
 *   Takuo KITAME <kitame@northeye.org>
 *
 */

#ifndef __DEDIT_MDI_H__
#define __DEDIT_MDI_H__

#include <gnome.h>
#include "dedit-document.h"

GnomeMDI * dedit_mdi_init (const gchar * appname, const gchar * title);
DEditDocument * dedit_mdi_get_active_document (GnomeMDI * mdi);

#endif /* __DEDIT_MDI_H__ */
