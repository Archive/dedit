/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * $Id$
 *
 * Copyright (C) 1999,2000,20001  Takuo KITAME
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License with
 * the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307  USA
 *
 * DEditDocment:
 *   DEdit Document Class
 *
 * Author:
 *   Takuo KITAME <kitame@northeye.org>
 *
 */

#ifndef __DEDIT_DOCUMENT_H__
#define __DEDIT_DOCUMENT_H__

#include <gnome.h>

#define D_DOCUMENT_TYPE     (d_document_get_type ())
#define D_DOCUMENT(obj)     ((GTK_CHECK_CAST ((obj), D_DOCUMENT_TYPE, DEditDocument)))
#define D_DOCUMENT_CLASS(klass) ((GTK_CHECK_CLASS_CAST((klass), D_DOCUMENT_TYPE, DEditDocumentClass)))
#define D_IS_DOCUMENT(obj)  (GTK_CHECK_TYPE((obj), D_DOCUMENT_TYPE))
#define D_IS_DOCUMENT_CLASS(klass)  (GTK_CHECK_CLASS_TYPE((klass), D_DOCUMENT_TYPE))
typedef struct _DEditDocument        DEditDocument;
typedef struct _DEditDocumentClass   DEditDocumentClass;
typedef struct _DEditDocumentPrivate DEditDocumentPrivate;

struct _DEditDocument {
    GtkObject parent;

    DEditDocumentPrivate *priv;
};

struct _DEditDocumentClass {
    GtkObjectClass parent_class;

    /* signals */
};

GType d_document_get_type                    (void) G_GNUC_CONST;
DEditDocument * dedit_document_new           (void);
DEditDocument * dedit_document_new_with_file (const gchar * filename);
void
dedit_document_set_file_name (DEditDocument *document, const gchar * filename);

void dedit_document_load_file (DEditDocument * document);

gboolean d_document_is_modified (DEditDocument * document);
const gchar * dedit_document_get_name (DEditDocument * document);
const gchar * dedit_document_get_path (DEditDocument * document);
gboolean dedit_document_is_modified (DEditDocument *document);

void dedit_document_grab_focus (DEditDocument *document);

void dedit_document_cut_clipboard (DEditDocument * document);
void dedit_document_copy_clipboard (DEditDocument * document);
void dedit_document_paste_clipboard (DEditDocument * document);
void dedit_document_select_all (DEditDocument * document);
void dedit_document_create_mdi_child (DEditDocument * document, GnomeMDI * mdi);

#endif
