/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 1999,2000,20001  Takuo KITAME
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License with
 * the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307  USA
 *
 * _DESCRIPTION_
 *
 * Author:
 *   Takuo KITAME <kitame@northeye.org>
 *
 */

#ifndef __DEDIT_FILE_DIALOG_H__
#define __DEDIT_FILE_DIALOG_H__

#include <gnome.h>
#include "dedit-utils.h"

#define D_FILE_DIALOG_TYPE     (d_file_dialog_get_type ())
#define D_FILE_DIALOG(obj)     ((GTK_CHECK_CAST ((obj), D_FILE_DIALOG_TYPE, DEditFileDialog)))
#define D_FILE_DIALOG_CLASS(klass) ((GTK_CHECK_CLASS_CAST((klass), D_FILE_DIALOG_TYPE, DEditFileDialogClass)))
#define D_IS_FILE_DIALOG(obj)  (GTK_CHECK_TYPE((obj), D_FILE_DIALOG_TYPE))
#define D_IS_FILE_DIALOG_CLASS(klass)  (GTK_CHECK_CLASS_TYPE((klass), D_FILE_DIALOG_TYPE))
typedef struct _DEditFileDialog        DEditFileDialog;
typedef struct _DEditFileDialogClass   DEditFileDialogClass;
typedef struct _DEditFileDialogPrivate DEditFileDialogPrivate;

struct _DEditFileDialog {
    GtkFileSelection parent;

    DEditFileDialogPrivate *priv;
};

struct _DEditFileDialogClass {
    GtkFileSelectionClass parent_class;

    /* signals */
};

GtkType d_file_dialog_get_type                  (void);

GtkWidget * dedit_file_dialog_new (const gchar * title);
DEditCharset dedit_file_dialog_get_charset (DEditFileDialog * dialog);
void dedit_file_dialog_set_charset (DEditFileDialog * dialog,
                                    DEditCharset charset);

#endif /* __DEDIT_FILE_DIALOG_H__*/
