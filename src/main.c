/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 1999,2000,20001  Takuo KITAME
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License with
 * the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307  USA
 *
 * The main function.
 *
 * Author:
 *   Takuo KITAME <kitame@northeye.org>
 *
 */

#include <config.h>

#include <gnome.h>

#include <libgnomevfs/gnome-vfs.h>

#include "dedit-mdi.h"
#include "dedit-gconf.h"

#include "dedit-utils.h"

#define PRODUCT_NAME "DEdit"

gint
main (gint argc, gchar **argv)
{
	GnomeMDI * mdi;
    gchar ** arg_files;
//	CORBA_ORB orb;
//	CORBA_Environment ev;
    poptContext ctx;
    
    TRACE_ENTER;

    if (g_getenv ("LANG") == NULL)
        setenv ("LANG", "C", 1);

    if (strcmp (g_getenv ("LANG"), "C") == 0 || 
        strcmp (g_getenv ("LANG"), "") == 0)
        unsetenv ("XMODIFIERS");

#ifdef ENABLE_NLS
    bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
    textdomain (GETTEXT_PACKAGE);
#endif

    gnome_init_with_popt_table ("DEdit2", VERSION, argc, argv,
								NULL, 0, &ctx);
//	CORBA_exception_init (&ev);
	if (!gnome_vfs_init ()) {
		g_error (_("Cannot initialize gnome-vfs."));
		return 1;
	}
	
    dedit_gconf_init (argc, argv);
    mdi = dedit_mdi_init (PACKAGE, PRODUCT_NAME);

	if (argc > 1) {
		arg_files = (gchar **)poptGetArgs(ctx);
		while (arg_files && *arg_files) {
			DEditDocument *document;
			document = dedit_document_new_with_file (*arg_files);
			dedit_document_create_mdi_child (document, mdi);
			dedit_document_load_file (document);
			arg_files ++;
		}
	} else {
		DEditDocument *document;
		document = dedit_document_new ();
		dedit_document_create_mdi_child (document, mdi);		
	}

	gtk_main ();
//	CORBA_exception_free (&ev);
    TRACE_EXIT;
    
    return 0;
}
