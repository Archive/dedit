static GnomeUIInfo filemenu[] = {
    {GNOME_APP_UI_ITEM, 
     N_("_New"), N_("New Document"),
     new_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
     'N', GDK_CONTROL_MASK, NULL},
    {GNOME_APP_UI_ITEM, 
     N_("_Open"), N_("Open Document"),
     open_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN,
     'O', GDK_CONTROL_MASK, NULL},
    {GNOME_APP_UI_ITEM, 
     N_("_Save"), N_("Save Document"),
     save_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE,
     'S', GDK_CONTROL_MASK, NULL},
    {GNOME_APP_UI_ITEM, 
     N_("Save _As..."), N_("Save Document As..."),
     save_as_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE_AS,
     0, 0, NULL},
    GNOMEUIINFO_SEPARATOR,
    {GNOME_APP_UI_ITEM, 
     N_("_Print..."), N_("Print Document..."),
     print_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PRINT,
     'P', GDK_CONTROL_MASK, NULL},
    GNOMEUIINFO_SEPARATOR,
    {GNOME_APP_UI_ITEM, 
     N_("_Close"), N_("Close Window"),
     close_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CLOSE,
     'D', GDK_CONTROL_MASK, NULL},
    {GNOME_APP_UI_ITEM, 
     N_("_Quit"), N_("Quit DEdit"),
     quit_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT,
     'Q', GDK_CONTROL_MASK, NULL},
    GNOMEUIINFO_END
};

/* edit menu */
static GnomeUIInfo editmenu[] = {
    {GNOME_APP_UI_ITEM, 
     N_("_Cut"), N_("Cut Selected Text"),
     cut_cb, NULL, NULL,
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CUT,
     'X', GDK_CONTROL_MASK, NULL},
    {GNOME_APP_UI_ITEM, 
     N_("C_opy"), N_("Cut Selected Text"),
     copy_cb, NULL, NULL,
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_COPY,
     'C', GDK_CONTROL_MASK, NULL},
    {GNOME_APP_UI_ITEM, 
     N_("_Paste"), N_("Paste Selected Text"),
     paste_cb, NULL, NULL,
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PASTE,
     'V', GDK_CONTROL_MASK, NULL},
    {GNOME_APP_UI_ITEM, 
     N_("Select _All"), N_("Select All Text"),
     select_all_cb, NULL, NULL,
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK,
     'A', GDK_CONTROL_MASK, NULL},
    GNOMEUIINFO_SEPARATOR,
//    GNOMEUIINFO_MENU_FIND_ITEM(find_dialog, NULL),
//    GNOMEUIINFO_MENU_FIND_AGAIN_ITEM(find_again_cb, NULL),
    GNOMEUIINFO_SEPARATOR,
    /*    {GNOME_APP_UI_TOGGLEITEM, N_("_Fixed Font"), 
     N_("Display text with a fixed font"),
     fixed_cb, NULL, NULL,
     0, 0, 'f', 
     GDK_CONTROL_MASK, NULL }, */
    GNOMEUIINFO_MENU_PREFERENCES_ITEM(prefs_cb, NULL),
    GNOMEUIINFO_END
};

static GnomeUIInfo emptymenu[] = {
    GNOMEUIINFO_END
};

/* help menu */
static GnomeUIInfo helpmenu[] = {
    GNOMEUIINFO_MENU_ABOUT_ITEM(about_cb,NULL),
    // GNOMEUIINFO_HELP("help-browser"),
    GNOMEUIINFO_END
};

/* menu bar */
static GnomeUIInfo mainmenu[] = {
    GNOMEUIINFO_SUBTREE(N_("_File"), filemenu),
    GNOMEUIINFO_SUBTREE(N_("_Edit"), editmenu),
    GNOMEUIINFO_SUBTREE(N_("_Documents"), emptymenu),
    GNOMEUIINFO_SUBTREE(N_("_Help"), helpmenu),
    GNOMEUIINFO_END
};

/* Tool bar */
static GnomeUIInfo toolbar[] = {
    {GNOME_APP_UI_ITEM, N_("New"), N_("New Document"),
     new_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_NEW,
     0, 0, NULL},
    {GNOME_APP_UI_ITEM, N_("Open"), N_("Open Document"),
     open_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_OPEN,
     0, 0, NULL},
    {GNOME_APP_UI_ITEM, N_("Save"), N_("Save Document"),
     save_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_SAVE,
     0, 0, NULL},
    {GNOME_APP_UI_ITEM, N_("Save As"), N_("Save Document As..."),
     save_as_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_SAVE_AS,
     0, 0, NULL},
    {GNOME_APP_UI_ITEM, N_("Print"), N_("Print Document..."),
     print_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_PRINT,
     0, 0, NULL},
    GNOMEUIINFO_SEPARATOR,
    {GNOME_APP_UI_ITEM, N_("Cut"), N_("Cut Selected Text"),
     cut_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_CUT,
     0, 0, NULL},
    {GNOME_APP_UI_ITEM, N_("Copy"), N_("Copy Selected Text"),
     copy_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_COPY,
     0, 0, NULL},
    {GNOME_APP_UI_ITEM, N_("Paste"), N_("Paste Buffered Text"),
     paste_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_PASTE,
     0, 0, NULL},
    GNOMEUIINFO_SEPARATOR,
    {GNOME_APP_UI_ITEM, N_("Preference"), N_("Preference"),
     prefs_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_PREFERENCES,
     0, 0, NULL},
    {GNOME_APP_UI_ITEM, N_("Close"), N_("Close Window"),
     close_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_CLOSE,
     0, 0, NULL},
    {GNOME_APP_UI_ITEM, N_("Quit"), N_("Quit DEdit"),
     quit_cb, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_EXIT,
     0, 0, NULL},
    GNOMEUIINFO_END
};

