/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 1999,2000,20001  Takuo KITAME
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License with
 * the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307  USA
 *
 * GConf functions.
 *
 * Author:
 *   Takuo KITAME <kitame@northeye.org>
 *
 */


#include <config.h>

#include <gconf/gconf-client.h>
#include <gconf/gconf.h>

#include <stdlib.h>

#include "dedit-gconf.h"
#include "dedit-utils.h"

#define DEDIT_GCONF_PATH "/apps/dedit"

static GConfClient * global_gconf_client = NULL;

static void
global_client_free (void)
{
	TRACE_ENTER;
	
	if (global_gconf_client == NULL) {
		TRACE_EXIT;
		return;
	}
	
	// gtk_object_unref (GTK_OBJECT (global_gconf_client));
	global_gconf_client = NULL;

	TRACE_EXIT;
	return;
}

/* Public */
GConfClient *
dedit_gconf_client_get_global (void)
{
	TRACE_ENTER;
	
	/* Initialize gconf if needed */
	if (!gconf_is_initialized ()) {
		gchar *argv[] = { "dedit", NULL };
		dedit_gconf_init (1, argv);
	}
	
	if (global_gconf_client == NULL) {
		global_gconf_client = gconf_client_get_default ();
		g_atexit (global_client_free);
	}

	TRACE_EXIT;
	return global_gconf_client;
}

/* bool */
void
dedit_gconf_set_bool (const gchar * key, gboolean value)
{
	GConfClient * client;
	GError * error = NULL;
	gchar path_key[PATH_MAX];

	TRACE_ENTER;
	
	g_return_if_fail (key != NULL);

	client = dedit_gconf_client_get_global ();
	g_return_if_fail (client != NULL);

	g_snprintf (path_key, sizeof (path_key), DEDIT_GCONF_PATH "/%s", key);
	
	gconf_client_set_bool (client, path_key, value, &error);

	/* FIXME: should place error handler */

	TRACE_EXIT;

	return;
}

gboolean
dedit_gconf_get_bool (const gchar * key)
{
	gboolean ret;
	GConfClient * client;
	GError * error = NULL;
	gchar path_key[PATH_MAX];

	TRACE_ENTER;
	
	g_return_val_if_fail (key != NULL, FALSE);

	client = dedit_gconf_client_get_global ();
	g_return_val_if_fail (client != NULL, FALSE);

	g_snprintf (path_key, sizeof (path_key), DEDIT_GCONF_PATH "/%s", key);
	
	ret = gconf_client_get_bool (client, path_key, &error);

	/* FIXME: should place error handler */

	TRACE_EXIT;

	return ret;
}

/* int */
void
dedit_gconf_set_int (const gchar * key, int value)
{
	GConfClient * client;
	GError * error = NULL;
	gchar path_key[PATH_MAX];

	TRACE_ENTER;
	
	g_return_if_fail (key != NULL);

	client = dedit_gconf_client_get_global ();
	g_return_if_fail (client != NULL);

	g_snprintf (path_key, sizeof (path_key), DEDIT_GCONF_PATH "/%s", key);
	
	gconf_client_set_int (client, path_key, value, &error);

	/* FIXME: should place error handler */

	TRACE_EXIT;

	return;
}

gint
dedit_gconf_get_int (const gchar * key)
{
	gint ret;
	GConfClient * client;
	GError * error = NULL;
	gchar path_key[PATH_MAX];

	TRACE_ENTER;
	
	g_return_val_if_fail (key != NULL, FALSE);

	client = dedit_gconf_client_get_global ();
	g_return_val_if_fail (client != NULL, FALSE);

	g_snprintf (path_key, sizeof (path_key), DEDIT_GCONF_PATH "/%s", key);
	
	ret = gconf_client_get_int (client, path_key, &error);

	/* FIXME: should place error handler */

	TRACE_EXIT;

	return ret;
}

/* string */
void
dedit_gconf_set_string (const gchar * key, gchar * value)
{
	GConfClient * client;
	GError * error = NULL;
	gchar path_key[PATH_MAX];

	TRACE_ENTER;
	
	g_return_if_fail (key != NULL);

	client = dedit_gconf_client_get_global ();
	g_return_if_fail (client != NULL);

	g_snprintf (path_key, sizeof (path_key), DEDIT_GCONF_PATH "/%s", key);
	
	gconf_client_set_string (client, path_key, value, &error);

	/* FIXME: should place error handler */

	TRACE_EXIT;

	return;
}

gchar *
dedit_gconf_get_string (const gchar * key)
{
	gchar * ret;
	GConfClient * client;
	GError * error = NULL;
	gchar path_key[PATH_MAX];

	TRACE_ENTER;
	
	g_return_val_if_fail (key != NULL, FALSE);

	client = dedit_gconf_client_get_global ();
	g_return_val_if_fail (client != NULL, FALSE);

	g_snprintf (path_key, sizeof (path_key), DEDIT_GCONF_PATH "/%s", key);
	
	ret = gconf_client_get_string (client, path_key, &error);

	/* FIXME: should place error handler */

	TRACE_EXIT;

	return ret;
}


void dedit_gconf_suggest_sync (void);

void
dedit_gconf_init (gint argc, gchar ** argv)
{
	GError * error;
	TRACE_ENTER;
	
    if (!gconf_init (argc, argv, &error)) {
        g_assert (error != NULL);
        g_message ("GConf init failed: %s", error->message);
        g_error_free (error);
        exit (EXIT_FAILURE);
    }
	
	TRACE_EXIT;

	return;
}
