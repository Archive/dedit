/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 1999,2000,20001  Takuo KITAME
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License with
 * the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307  USA
 *
 * Some utility functions.
 *
 * Author:
 *   Takuo KITAME <kitame@northeye.org>
 *
 */

#include <config.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "dedit-utils.h"

/* get absolute path (only directory) from filename */
gchar *
g_get_absolute_file_path (const gchar * filename)
{
    gchar resolved_path[PATH_MAX];
    gchar opath[PATH_MAX];
    gchar * op, * bp, * p;
    
	TRACE_ENTER;
    
    strcpy (opath, filename);
    bp = strrchr (opath, '/');
    if (bp == NULL) {
        op = ".";
        bp = opath;
    } else {
        *bp++ = '\0';
        op = opath;
    }

    p = realpath (op, resolved_path);

    if (!p) {
		TRACE_EXIT;
        return NULL;
	}
    
	D (("*** DEBUG: %s\n", p));
	
	TRACE_EXIT;

    return g_strdup_printf ("%s", p);
}

/* get absolute path from filename */
gchar *
g_get_absolute_file_name (const gchar * filename)
{
	gchar * retval;
	gchar * absolute_path;
    gchar opath[PATH_MAX];
    gchar * bp;
    
	TRACE_ENTER;

	g_return_val_if_fail (filename != NULL, NULL);
	
    strcpy (opath, filename);
    bp = strrchr (opath, '/');

    if (bp == NULL) {
        bp = opath;
    } else {
        *bp++ = '\0';
    }

	absolute_path = g_get_absolute_file_path (filename);
	g_return_val_if_fail (absolute_path != NULL, NULL);

	retval = g_strdup_printf ("%s/%s", absolute_path, bp);
	g_free (absolute_path);
    
	D (("*** DEBUG: %s\n", retval));

	TRACE_EXIT;
	
    return retval;
}
